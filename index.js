
// Dependencies
const Promise = require('bluebird');
const os = require('os');
const fs = require('fs');
const stat = Promise.promisify(fs.stat);
const path = require('path');
const process = require('process');
const execFile = Promise.promisify(require('child_process').execFile);
const Request = require('request');
const winston = require('winston');

// Constants
const defaultDownloadUrl = "https://github.com/git-for-windows/git/releases/download/v2.9.2.windows.1/PortableGit-2.9.2-32-bit.7z.exe";
const defaultTempFilename = "PortableGit.7z.exe";
const portableGitRoot = path.join(process.env.APPDATA, 'PortableGit');
const GIT_BIN = path.join(portableGitRoot, 'bin', 'git.exe');

// Helpers
winston.level = 'debug';

const log = winston.info.bind(winston);



// Download git-portable
function downloadFile(downloadUrl = defaultDownloadUrl){
    const tmpFilePath = path.join(process.env.APPDATA, defaultTempFilename);
    log(tmpFilePath);
    const dlStream = Request(downloadUrl)
        .pipe(fs.createWriteStream(tmpFilePath));

    return new Promise(function(res, rej){
        dlStream.on('finish', ()=> res(tmpFilePath));
        dlStream.on('error', rej);
    });
}


// Execute git 7z extractor
function installPortableGit(installerTmpLocation) {
    // Spawn a shell to install the file
    // May require setting the working directory

    const options = [
        '-y',
        '-gm2'
    ];

    const command = `${installerTmpLocation} ${options.join(' ')}`;
    log(command);

    return execFile(installerTmpLocation, options, {cwd: process.cwd()})
        .then(()=>GIT_BIN);

}

// Check if we have GIT where we think we should, otherwise download it
function getGitPath() {
    // Does GIT_BIN Exist?
    return stat(GIT_BIN)
        .then(()=>GIT_BIN)                  // Yes, return the path to it
        .catch(()=> {                       // No:
            return downloadFile()           //    - Download it
                .then(installPortableGit);   //    - install it, and return the path to it
        });
}




function git(options = {}) {
    return (...args) => getGitPath()
        .then((git_path) => {
            return execFile(git_path, args, options);
        })
        .catch(log);
}

module.exports = git;